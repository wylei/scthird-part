#
# Be sure to run `pod lib lint SCThirdPartCocoaPods.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SCThirdPartCocoaPods'
  s.version          = '0.3.0'
  s.summary          = 'SCThirdPartCocoaPods'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  SCThirdPartCocoaPods 控件
                       DESC

  s.homepage         = 'https://github.com/wangyalei2016@yeah.net/SCThirdPartCocoaPods'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'wangyalei' => 'wangyalei2016@yeah.net' }
  s.source           = { :git => 'https://gitee.com/wylei/scthird-part.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  pch_Common = <<-EOS
  #import "SCThirdPartCocoaPodsDefines.h"
  EOS
  s.prefix_header_contents = pch_Common
  
  s.ios.deployment_target = '9.0'

  s.source_files = 'SCThirdPartCocoaPods/Classes/**/*'
  
  # s.resource_bundles = {
  #   'SCThirdPartCocoaPods' => ['SCThirdPartCocoaPods/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
