//
//  SCViewController.m
//  SCThirdPartCocoaPods
//
//  Created by wangyalei2016@yeah.net on 01/13/2021.
//  Copyright (c) 2021 wangyalei2016@yeah.net. All rights reserved.
//

#import "SCViewController.h"

#import <SCThirdPartCocoaPods/SCThirdPartCocoaPodsDefines.h>


@interface SCViewController ()

@end

@implementation SCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    HWTFCodeBView *codeView = [[HWTFCodeBView alloc] initWithCount:5 margin:200];
    codeView.frame = CGRectMake(10, 100, 200, 50);
    codeView.backgroundColor = UIColor.redColor;
    [self.view addSubview:codeView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
