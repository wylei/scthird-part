//
//  main.m
//  SCThirdPartCocoaPods
//
//  Created by wangyalei2016@yeah.net on 01/13/2021.
//  Copyright (c) 2021 wangyalei2016@yeah.net. All rights reserved.
//

@import UIKit;
#import "SCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SCAppDelegate class]));
    }
}
