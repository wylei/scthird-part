//
//  SCAppDelegate.h
//  SCThirdPartCocoaPods
//
//  Created by wangyalei2016@yeah.net on 01/13/2021.
//  Copyright (c) 2021 wangyalei2016@yeah.net. All rights reserved.
//

@import UIKit;

@interface SCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
