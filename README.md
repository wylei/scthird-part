# SCThirdPartCocoaPods

[![CI Status](https://img.shields.io/travis/wangyalei2016@yeah.net/SCThirdPartCocoaPods.svg?style=flat)](https://travis-ci.org/wangyalei2016@yeah.net/SCThirdPartCocoaPods)
[![Version](https://img.shields.io/cocoapods/v/SCThirdPartCocoaPods.svg?style=flat)](https://cocoapods.org/pods/SCThirdPartCocoaPods)
[![License](https://img.shields.io/cocoapods/l/SCThirdPartCocoaPods.svg?style=flat)](https://cocoapods.org/pods/SCThirdPartCocoaPods)
[![Platform](https://img.shields.io/cocoapods/p/SCThirdPartCocoaPods.svg?style=flat)](https://cocoapods.org/pods/SCThirdPartCocoaPods)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SCThirdPartCocoaPods is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SCThirdPartCocoaPods'
```

## Author

wangyalei2016@yeah.net, wangyalei2016@yeah.net

## License

SCThirdPartCocoaPods is available under the MIT license. See the LICENSE file for more info.
